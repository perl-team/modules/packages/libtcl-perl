Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://metacpan.org/release/Tcl
Upstream-Contact: Vadim Konovalov <vkon@cpan.org>
Upstream-Name: Tcl
Files-Excluded: tcl-core
Comment: The tarball is repacked to get rid of the tcl-core directory which
 contains pre-compiled Tcl libraries.

Files: *
Copyright: 1994-1997, Malcolm Beattie
License: Artistic or GPL-1+

Files: Tcl.xs
Copyright: 1994-1997, Malcolm Beattie
 2003-2024, Vadim Konovalov
 2004, ActiveState Corp., a division of Sophos PLC
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2014-2024, gregor herrmann <gregoa@debian.org>
 2014, Damyan Ivanov <dmn@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

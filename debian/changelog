libtcl-perl (1.32+ds-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Update standards version to 4.6.2, no changes needed.

  [ gregor herrmann ]
  * Import upstream version 1.32+ds.
  * Update years of upstream and packaging copyright.
  * Install new Tcl-hack.pod document into /usr/share/doc/libtcl-perl.

 -- gregor herrmann <gregoa@debian.org>  Sun, 21 Jan 2024 00:46:13 +0100

libtcl-perl (1.27+ds-2) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Remove obsolete field Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Update standards version to 4.5.0, no changes needed.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.
  * Update standards version to 4.6.0, no changes needed.

  [ gregor herrmann ]
  * Update debian/upstream/metadata.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.1.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Mon, 15 Aug 2022 16:14:58 +0200

libtcl-perl (1.27+ds-1) unstable; urgency=medium

  * Import upstream version 1.27+ds.
  * Update debian/upstream/metadata.
  * Declare compliance with Debian Policy 4.3.0.
  * Add build dependency on libdevel-refcount-perl for another test.

 -- gregor herrmann <gregoa@debian.org>  Wed, 26 Dec 2018 19:37:44 +0100

libtcl-perl (1.25+ds-1) unstable; urgency=medium

  * Import upstream version 1.25+ds.
  * Drop 01_fix-spelling-errors-in-manpage.patch, applied upstream.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Fri, 27 Jul 2018 20:53:15 +0200

libtcl-perl (1.23+ds-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Lucas Kanashiro ]
  * New upstream version 1.23+ds
  * Bump debhelper compatibility level to 11
  * Declare complaince with Debian Policy 4.1.5
  * Update years of upstream copyright
  * Remove patches already applied by upstream
  * Create a patch to fix spelling errors in manpage

 -- Lucas Kanashiro <kanashiro@debian.org>  Mon, 23 Jul 2018 08:49:49 -0300

libtcl-perl (1.05+ds-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Change bugtracker URL(s) to HTTPS.
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.

  [ intrigeri ]
  * Switch repackaging framework to Files-Excluded method.
  * Imported Upstream version 1.05+ds

 -- intrigeri <intrigeri@boum.org>  Wed, 29 Jun 2016 12:52:20 +0000

libtcl-perl (1.04+ds-1) unstable; urgency=medium

  * Team upload

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ Florian Schlichting ]
  * Add debian/upstream/metadata
  * Import Upstream version 1.04+ds
  * Drop wording.patch (applied upstream) and refresh
    fix_hardening_FTBFS.patch
  * Declare compliance with Debian Policy 3.9.8
  * Enable all hardening flags
  * Add spelling.patch
  * Mark package autopkgtest-able

 -- Florian Schlichting <fsfs@debian.org>  Sun, 24 Apr 2016 01:09:47 +0200

libtcl-perl (1.02+ds-2) unstable; urgency=medium

  * Team upload.
  * Add patch to pass format string as %s to allow for compiling with
    hardnening options (Closes: #752587)

 -- Julián Moreno Patiño <julian@debian.org>  Tue, 24 Jun 2014 22:36:51 -0500

libtcl-perl (1.02+ds-1) unstable; urgency=low

  * Initial release.
    (Closes: #750826: ITP: libtcl-perl -- Tcl extension module for Perl)

 -- gregor herrmann <gregoa@debian.org>  Sat, 07 Jun 2014 13:20:30 +0200
